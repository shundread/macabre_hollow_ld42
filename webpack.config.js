const typescriptLoader = {
  loader: 'ts-loader',
};

const babelLoader = {
  loader: 'babel-loader',
  options: {
    presets: ['env'],
  },
};

module.exports = {
  devtool: 'source-map',
  entry: './src/main.ts',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [ babelLoader, typescriptLoader ],
      },
    ],
  },
  output: {
    filename: 'bundle.js', // '[name].[chunkhash:8].js'
  },
  resolve: {
   extensions: [ '.ts', '.js' ]
  },
}
