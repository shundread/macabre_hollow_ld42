// External Libraries
import * as PIXI from "pixi.js";
import { Easing, Tween } from "tween.js";

// Game libraries
import { InstructionsTextStyle } from "./textStyle";

const FullWidth: number = 0.8;
const Padding: number = 0.1;

export class InstructionsPage extends PIXI.Container {
  // PIXI resources;
  private background: PIXI.Graphics;
  private text: PIXI.Text;

  // State information
  private locked: boolean = false;
  private pHeight: number;

  constructor(text: string, width: number, height: number) {
    super();

    this.pHeight = height;

    this.interactive = true;
    this.on("pointerdown", this.press);

    this.background = this.makeBackground(width, height);
    this.text = new PIXI.Text(text, InstructionsTextStyle);

    this.addChild(this.background);
    this.addChild(this.text);

    this.arrangeAssets(width, height);

    this.visible = false;
  }

  public resize(width: number, height: number) {
    this.pHeight = height;

    this.removeChild(this.background);
    this.background.destroy();
    this.removeChild(this.text);
    this.background = this.makeBackground(width, height);
    this.addChild(this.background);
    this.addChild(this.text);

    this.arrangeAssets(width, height);
  }

  public show() {
    this.text.position.y = this.pHeight * Padding;
    this.visible = true;
  }

  private arrangeAssets(width: number, height: number) {
    this.text.width = Math.min(this.text.width, width * FullWidth);
    this.text.position.set(
      0.5 * (width - this.text.width),
      height * Padding);
  }

  private makeBackground(width: number, height: number): PIXI.Graphics {
    const frame = new PIXI.Graphics();

    // Draw rectangle
    frame.beginFill(0xeeeeee);
    frame.drawRect(0, 0, width, height);
    frame.endFill();

    return frame;
  }

  private press() {
    if (this.locked) {
      return;
    }

    // Scroll a quarter of a page down if we still got text to show
    if (this.text.position.y + this.text.height > this.pHeight) {
      this.locked = true;

      // const currentY = this.text.position.y;
      new Tween(this.text.position)
        .to({ y: this.text.position.y - this.pHeight * 0.25 }, 250)
        .easing(Easing.Quadratic.InOut)
        .onComplete(() => this.unlock())
        .start();

    // Dismiss the page otherwise
    } else {
      this.visible = false;
    }
  }

  private unlock() {
    this.locked = false;
  }
}
