// External Libraries
import * as PIXI from "pixi.js";
import { Easing, Tween } from "tween.js";

// Game libraries
import { Button } from "./button";
import * as IMAGE from "./image";
import { InstructionsPage } from "./instructionsPage";
import { lastLevelIndex } from "./level";
import { MenuTextStyle } from "./textStyle";

const FullWidth: number = 0.8;
const SmallWidth: number = 0.1;
const StartWidth: number = 0.40;
const RowHeight: number = 0.15;
const VerticalPad: number = (1 - (5 * RowHeight)) / 6;
const LeftPad: number = 0.5 * (1 - FullWidth);
const SmallPad: number = 0.1 / 3;

const rulesText: string = `
There are a few simple rules to this game:

* Each character moves once per turn

* Attempting to move on top of another character
  damages the other character

* Moving on top of an item uses the item

* Enemies also use items

* Enemies always try to approach the player without
  without thinking about going around
  obstacles

* Enemies always prefer horizontal movements to
  vertical movements

* Every attribute fits a single decimal

* No attribute can be negative

* Understanding the data structure of the attributes
  may help you
`;

const creditsText: string = `
  Concept, programming, level design and art:

        Thiago Chaves de Oliveira Horta
`;

const structureText: string = `
I noticed that the MAX_SAFE_INTEGER constant in JS/TS
is a pretty large number, there are 16 decimals in it,
and with the exception of the most significant digit,
all of them can range from 0 to 9.

With that many digits, it seems like you can actually
squeeze in a lot of independent data into a single
primitive variable. If you limit the set of attributes
for each character to 3, and limit them to be at most
9 each, you can actually fit data for 5 characters in
a single primitive variable!

 vvvvv                                            vvvvv
  vvv   data for 5 characters in a single number   vvv
   v                                                v
+-----------------+-----------+--------+--------+--------+
|      Player     |   Enemy2  | Enemy3 | Enemy4 | Enemy5 |
+-----------------+-----------+--------+--------+--------+
|Type Power Health| T   P   H | T  P  H| T  P  H| T  P  H|
+----+-----+------+---+---+---+--+--+--+--+--+--+--+--+--+
| 1-9| 0-9 |  0-9 |1-9|0-9|0-9|  |  |  |  |  |  |  |  |  |
+----+-----+------+---+---+---+--+--+--+--+--+--+--+--+--+

 +-------------+                         +-------------+
 |    least    |        confusing        |     most    |
 | significant |  -------------------->  | significant |
 |   digits    |         detail          |    digits   |
 +-------------+          sorry          +-------------+


This is important, because it saves memory, and it also
saves the user from having to think about those difficult
carry one operations:

                      8
                     +4
                    -----    <--- too complicated
                     12

Hell, this design is so clever that even I didn't have to
think about carry one operations once. I just ignored it
like a boss because every number in the game is so small.

The only detail I have to make sure was that no digit ever
goes negative. THAT would mess up the whole number. I
guarantee you there are no bugs in subtraction operations,
and that subtraction operations will not cause strange bugs
in the game.

If any space agencies are impressed with the reliability
of my software's math and want to hire me for writing
rocket software, do contact me. I'm willing to hear your
proposal.
`;

export class MainMenu extends PIXI.Container {
  public levelSelected: number = 1; // the level to be started if "start level" is pressed

  private title: PIXI.Sprite;

  private buttons: {
    levelBegin: Button,
    levelDown: Button,
    levelUp: Button,
    infoCredits: Button,
    infoRules: Button,
    infoStructure: Button,

    [name: string]: Button,
  };

  private levelIndicator: PIXI.Text;
  private highestLevel: number = 1; // the highest level that the player can start

  private onLevelStart: () => void;

  private instructions: {
    credits: InstructionsPage
    rules: InstructionsPage,
    structure: InstructionsPage,
  };

  constructor(width: number, height: number, onLevelStart: () => void) {
    super();

    this.updateHighestLevel();

    const fullWidth = FullWidth * width;
    const smallWidth = SmallWidth * width;
    const startWidth = StartWidth * width;
    const rowHeight = RowHeight * height;

    this.title = new PIXI.Sprite(IMAGE.get("title"));
    this.title.anchor.set(0.5, 0.5);
    this.addChild(this.title);

    this.buttons = {
      infoCredits: new Button(() => this.showCredits(), fullWidth, rowHeight, "credits"),
      infoRules: new Button(() => this.showRules(), fullWidth, rowHeight, "game rules"),
      infoStructure: new Button(() => this.showStructure(), fullWidth, rowHeight, "clever data structure"),
      levelBegin: new Button(() => this.startLevel(), startWidth, rowHeight, "start level"),
      levelDown: new Button(() => this.levelDown(), smallWidth, rowHeight, "<"),
      levelUp: new Button(() => this.levelUp(), smallWidth, rowHeight, ">"),
    };

    for (const id in this.buttons) { // tslint:disable-line:forin
      this.addChild(this.buttons[id]);
    }
    this.levelIndicator = new PIXI.Text(`${this.levelSelected}/${this.highestLevel}`, MenuTextStyle);
    this.levelIndicator.anchor.set(0.5, 0.5);
    this.addChild(this.levelIndicator);

    // Create instructions pages
    this.instructions = {
      credits: new InstructionsPage(creditsText, width, height),
      rules: new InstructionsPage(rulesText, width, height),
      structure: new InstructionsPage(structureText, width, height),
    };
    this.addChild(this.instructions.rules);
    this.addChild(this.instructions.credits);
    this.addChild(this.instructions.structure);

    this.arrangeAssets(width, height);
    this.updateButtonsActiveFlag();

    this.onLevelStart = onLevelStart;
    this.visible = false;
  }

  public resize(width: number, height: number) {
    const fullWidth = FullWidth * width;
    const smallWidth = SmallWidth * width;
    const startWidth = StartWidth * width;
    const rowHeight = RowHeight * height;

    this.buttons.levelBegin.resize(startWidth, rowHeight);
    this.buttons.levelDown.resize(smallWidth, rowHeight);
    this.buttons.levelUp.resize(smallWidth, rowHeight);
    this.buttons.infoRules.resize(fullWidth, rowHeight);
    this.buttons.infoCredits.resize(fullWidth, rowHeight);
    this.buttons.infoStructure.resize(fullWidth, rowHeight);
    this.arrangeAssets(width, height);

    this.instructions.structure.resize(width, height);
  }

  public show() {
    this.alpha = 0;
    this.visible = true;
    this.updateHighestLevel();
    this.levelIndicator.text = `${this.levelSelected}/${this.highestLevel}`;
    this.updateButtonsActiveFlag();

    new Tween(this)
      .to({ alpha: 1 }, 500)
      .easing(Easing.Quadratic.In)
      .start();
  }

  public startLevel() {
    new Tween(this)
      .to({ alpha: 0 }, 500)
      .easing(Easing.Quadratic.In)
      .onComplete(() => {
        this.visible = false;
        this.onLevelStart();
      })
      .start();
  }

  private arrangeAssets(width: number, height: number) {
    this.title.position.set(0.5 * width, rowY(1, height) + 0.5 * RowHeight * height);
    this.buttons.levelBegin.position.set(width * LeftPad, rowY(2, height));
    this.buttons.levelDown.position.set(
      width * (LeftPad + StartWidth + SmallPad),
      rowY(2, height));
    this.levelIndicator.position.set(
       // These coordinates look odd because the text doesn't include the large
       // frame that the buttons do, and would center at the top-left of what
       // could have been the frame
      width * (LeftPad + StartWidth + 1.5 * SmallWidth + 2 * SmallPad),
      rowY(2, height) + 0.5 * RowHeight * height);
    this.buttons.levelUp.position.set(
      width * (LeftPad + StartWidth + 2 * SmallWidth + 3 * SmallPad),
      rowY(2, height));
    this.buttons.infoCredits.position.set(width * LeftPad, rowY(3, height));
    this.buttons.infoRules.position.set(width * LeftPad, rowY(4, height));
    this.buttons.infoStructure.position.set(width * LeftPad, rowY(5, height));
  }

  private levelDown() {
    this.levelSelected -= 1;
    this.levelIndicator.text = `${this.levelSelected}/${this.highestLevel}`;
    this.updateButtonsActiveFlag();
  }

  private levelUp() {
    this.levelSelected += 1;
    this.levelIndicator.text = `${this.levelSelected}/${this.highestLevel}`;
    this.updateButtonsActiveFlag();
  }

  private showCredits() {
    this.instructions.credits.show();
  }

  private showRules() {
    this.instructions.rules.show();
  }

  private showStructure() {
    this.instructions.structure.show();
  }

  private updateButtonsActiveFlag() {
    this.buttons.levelDown.setActive(this.levelSelected > 1);
    this.buttons.levelUp.setActive(this.levelSelected < this.highestLevel);
  }

  private updateHighestLevel() {
    let levelsBeat = Number(window.localStorage.getItem("levelsBeat"));
    if (isNaN(levelsBeat)) { levelsBeat = 0; }

    this.highestLevel = Math.min(levelsBeat + 1, lastLevelIndex());
  }
}

function rowY(row: number, height: number) {
  return height * (VerticalPad + (row - 1)  * (VerticalPad + RowHeight));
}
