import * as PIXI from "pixi.js";

// tslint:disable:object-literal-sort-keys
const Attributes: {[index: string]: number} = {
  hero:     731,
  skeleton: 322,
  zombie:   523,
  ghoul:    634,
  devourer: 445,
  bloated:  936,
  reaper:   777,
  dragon:   888,
  chicken:  119,
};
// tslint:enable:object-literal-sort-keys

const Levels: string[][] = [
  // Level 1
  [
    "......#..",
    ".........",
    "..#.....#",
    ".....##..",
    "....H#X..",
    "..#..##..",
    ".#.......",
    ".........",
    "...##....",
  ],

  // Level 2
  [
    ".........",
    ".........",
    ".H.....S.",
    ".........",
    "#####....",
    ".........",
    ".X....rS.",
    ".........",
    ".........",
  ],

  // Level 3
  [
    "#########",
    "#..ZZZ.X#",
    "####.####",
    "####.####",
    "####.####",
    "####.####",
    "####.####",
    "####.####",
    "####H####",
  ],

  // Level 4
  [
    "#########",
    "#...#...#",
    "#H..w..D#",
    "#...#...#",
    "######w##",
    "#...#...#",
    "#X..w...#",
    "#.D.#...#",
    "#########",
  ],

  // Level 5
  [
    "......X..",
    "..R......",
    "...#.#...",
    "...###...",
    ".........",
    "...###..R",
    "...#.#...",
    "..H......",
    ".........",
  ],

  // Level 6
  [
    ".........",
    ".........",
    ".........",
    "#########",
    "rrH.GGGGX",
    "#########",
    ".........",
    ".........",
    ".........",
  ],

  // Level 7
  [
    ".........",
    ".........",
    ".........",
    "#########",
    "m.H.BRZ.X",
    "#########",
    ".........",
    ".........",
    ".........",
  ],

  // Level 8
  [
    "#####R...",
    "....#....",
    "....#....",
    "Hrrr.....",
    "#####p...",
    "RppX#p...",
    "....#....",
    "...h#....",
    "...hp...p",
  ],

  // Level 9
  [
    "..#.X....",
    "..#..pppB",
    ".##.a####",
    "..#a.....",
    "..#..a...",
    "#.#.a....",
    "..#.....H",
    "..#......",
    "..#......",
  ],

  // Level 10
  [
    "....X....",
    ".gprL.pp.",
    "####m####",
    "........R",
    ".#.#g#.#.",
    "...D.....",
    "r#######.",
    "......D.r",
    "....H...r",
  ],
];

export class Item {
  public name: string;
  public position: PIXI.Point;

  constructor(name: string, x: number, y: number) {
    this.name = name;
    this.position = new PIXI.Point(x, y);
  }
}

export interface ILevelData {
  attributes: number;
  characterPositions: PIXI.Point[];
  exit: PIXI.Point;
  items: Item[];
  tiles: string[][];
}

export function get(index: number): ILevelData {
  console.log("getting level", Levels[index - 1]);

  let attributes: number = Attributes.hero; // hero is always present, at slot 0
  const characterPositions: PIXI.Point[] = [new PIXI.Point(-1, 0)];
  const items: Item[] = [];
  const tiles: string[][] = [];
  const exit: PIXI.Point = new PIXI.Point(-1, 0);

  for (let y = 0; y < 9; y++) {
    tiles.push([]);
    for (let x = 0; x < 9; x++) {
      const code: string = Levels[index][y][x];

      // Terrain resolution
      if (code === "#") {
        tiles[y].push("wall");
        continue;
      } else {
        tiles[y].push("dirt");
      }

      // Exit resolution
      if (code === "X") { exit.set(x, y); }

      //
      // Character resolution
      //
      let characterAttributes: number = 0;

      // slot 0 reserved for player
      if (code === "H") { characterPositions[0].set(x, y); }

      // other characters have attributs + position added to the list
      if (code === "S") { characterAttributes = Attributes.skeleton; }
      if (code === "Z") { characterAttributes = Attributes.zombie; }
      if (code === "G") { characterAttributes = Attributes.ghoul; }
      if (code === "D") { characterAttributes = Attributes.devourer; }
      if (code === "B") { characterAttributes = Attributes.bloated; }
      if (code === "R") { characterAttributes = Attributes.reaper; }
      if (code === "L") { characterAttributes = Attributes.dragon; }
      if (code === "C") { characterAttributes = Attributes.chicken; }
      if (characterAttributes) {
        // calculate shifted character attributes and add to collective attributes
        const attrFactor = Math.pow(1000, characterPositions.length);
        attributes += characterAttributes * attrFactor;

        characterPositions.push(new PIXI.Point(x, y));
      }

      //
      // Item resolution
      //
      let itemName: string = "";
      if (code === "r") { itemName = "restore"; }
      if (code === "a") { itemName = "acid"; }
      if (code === "p") { itemName = "powerup"; }
      if (code === "w") { itemName = "weaken"; }
      if (code === "m") { itemName = "megacure"; }
      if (code === "g") { itemName = "cursedglyph"; }
      if (itemName !== "") {
        items.push(new Item(itemName, x, y));
        console.log(`added item '${items[items.length - 1].name}' to ${x}, ${y}`);
      }
    }
  }

  if (characterPositions[0].x === -1) {
    throw Error(`No hero found in map ${index}`);
  }
  if (exit.x === -1) {
    throw Error(`No exit found in map ${index}`);
  }

  return {
    attributes,
    characterPositions,
    exit,
    items,
    tiles,
  };
}

export function lastLevelIndex(): number {
  return Levels.length;
}
