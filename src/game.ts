// External Libraries
import * as PIXI from "pixi.js";
import { Easing, Tween } from "tween.js";

// Game libraries
import * as IMAGE from "./image";
import * as LEVEL from "./level";

import { Button } from "./button";
import { Entity } from "./entity";
import { StatusBarTextStyle } from "./textStyle";
import { colorToTint } from "./util";

const CharacterNames: {[index: number]: string } = {
  0: "",
  1: "Hero",
  2: "Skeleton",
  3: "Zombie",
  4: "Ghoul",
  5: "Devourer",
  6: "Bloated",
  7: "Reaper",
  8: "Dragon",
  9: "Chicken", // Definitely unused
};

// This is so much the wrong type for all this stuff
export class Game extends PIXI.Container {
  // Game data
  //                           edcba9876543210
  private attributes: number = 765654543432321; // All character attributes
  private characters: Entity[] = [];
  private exit: Entity;   // screw it, let's just implemente these
  private items: Entity[] = [];  // in terms of Entity
  private walls: boolean[][];
  private level: number = 0;

  // turn-taking
  private turn: number = 0; // index of character who owns the current turn

  //
  // HUD
  //
  private hudBackground: PIXI.Graphics;
  private buttons: {
    east: Button,
    north: Button,
    south: Button,
    wait: Button,
    west: Button,

    [name: string]: Button,
  };
  private buttonBox: PIXI.Container; // 120x120 pixels

  // Status bar
  private statusBarBackground: PIXI.Graphics;
  private statusBarText: PIXI.Text;
  private logs: string[] = [];

  // Map view
  private mapView: PIXI.Container; // 288x288 pixels
  private tiles: PIXI.Sprite[][]; // 9x9 tiles

  // Game exit callback
  private levelExit: (success: boolean, firstTime?: boolean) => void;

  constructor(width: number, height: number, onExit: (success: boolean, firstTime?: boolean) => void) {
    super();

    this.levelExit = onExit;

    // Make HUD
    this.hudBackground = new PIXI.Graphics();
    this.addChild(this.hudBackground);

    //
    // Input buttons
    //
    this.buttons = {
      east: new Button(() => this.goEast(), 32, 32, "E"),
      north: new Button(() => this.goNorth(), 32, 32, "N"),
      south: new Button(() => this.goSouth(), 32, 32, "S"),
      wait: new Button(() => this.goWait(), 32, 32, " "),
      west: new Button(() => this.goWest(), 32, 32, "W"),
    };

    this.buttonBox = new PIXI.Container();
    this.buttonBox.addChild(this.buttons.east);
    this.buttonBox.addChild(this.buttons.north);
    this.buttonBox.addChild(this.buttons.south);
    this.buttonBox.addChild(this.buttons.wait);
    this.buttonBox.addChild(this.buttons.west);

    // These need to be arranged only once, since they are arranged in relation
    // to the container buttonBox
    this.buttons.north.position.set(44, 0);
    this.buttons.west.position.set(0, 44);
    this.buttons.wait.position.set(44, 44);
    this.buttons.east.position.set(88, 44);
    this.buttons.south.position.set(44, 88);

    //
    // Attribute display statusBar
    //
    this.statusBarBackground = new PIXI.Graphics();
    this.statusBarBackground.lineStyle(2, 0x444444, 1);
    this.statusBarBackground.beginFill(0x000000);
    this.statusBarBackground.drawRect(0, 0, 180, 180);
    this.statusBarBackground.endFill();

    this.statusBarText = new PIXI.Text("", StatusBarTextStyle);
    this.updateStatusText();
    this.statusBarBackground.addChild(this.statusBarText);
    this.statusBarText.position.set(5, 5);

    //
    // Map view
    //
    this.mapView = new PIXI.Container();

    const mapFrame: PIXI.Graphics = new PIXI.Graphics();
    mapFrame.lineStyle(2, 0x444444, 1);
    mapFrame.beginFill(0x000000);
    mapFrame.drawRect(0, 0, 288, 288);
    mapFrame.endFill();
    this.mapView.addChild(mapFrame);

    // Logical & Graphical map data
    this.walls = [];
    this.tiles = [];
    for (let y = 0; y < 9; y++) {
      this.walls.push([]);
      this.tiles.push([]);
      for (let x = 0; x < 9; x++) {
        this.walls[y].push(true);
        const tile = new PIXI.Sprite(IMAGE.get("wall"));
        this.mapView.addChild(tile);
        this.tiles[y].push(tile);
        tile.position.set(x * 32, y * 32);
      }
    }

    // Exit entity
    this.exit = new Entity("exit", new PIXI.Point(0, 0));
    this.mapView.addChild(this.exit);

    // Add container assets
    this.addChild(this.buttonBox);
    this.addChild(this.statusBarBackground);
    this.addChild(this.mapView);
    this.arrangeAssets(width, height);
    this.visible = false;
  }

  public resize(width: number, height: number) {
    this.arrangeAssets(width, height);
  }

  public enterLevel(level: number) {
    this.logs = []; // clear any lingering logs from previous runs
    this.level = level;
    const data: LEVEL.ILevelData = LEVEL.get(level - 1);

    this.putAttributes(data.attributes);

    // Harden agaisnt z-sorting problems if implementation of these utility
    // functions are changed to destroy/recreate sprites
    this.putTerrain(data.tiles);
    this.putExit(data.exit);
    this.putItems(data.items);
    this.putCharacters(data.characterPositions);

    this.alpha = 0;
    this.visible = true;
    this.buttonsEnable();
    new Tween(this)
      .to({ alpha: 1 }, 500)
      .easing(Easing.Quadratic.In)
      .start();
  }

  //
  // Layout management
  //

  private arrangeAssets(width: number, height: number) {
    this.hudBackground.clear();
    if (width > height) {
      this.arrangeAssetsLandscape(width, height);
    } else {
      this.arrangeAssetsVertical(width, height);
    }
  }

  private arrangeAssetsLandscape(width: number, height: number) {
    this.hudBackground.lineStyle(2, 0x444444, 1);
    this.hudBackground.beginFill(0x222222);
    this.hudBackground.drawRect(width - 220, 0, 320, height);
    this.hudBackground.endFill();

    this.buttonBox.position.set(width - 170, height - 150);
    this.statusBarBackground.position.set(width - 200, 20);
    this.mapView.position.set(0.5 * (width - 220 - 288), 0.5 * (height - 288));
  }

  private arrangeAssetsVertical(width: number, height: number) {
    this.hudBackground.lineStyle(2, 0x444444, 1);
    this.hudBackground.beginFill(0x222222);
    this.hudBackground.drawRect(0, height - 220, width, height);
    this.hudBackground.endFill();

    this.buttonBox.position.set(width - 150, height - 170);
    this.statusBarBackground.position.set(20, height - 200);
    this.mapView.position.set(0.5 * (width - 288), 0.5 * (height - 220 - 288));
  }

  //
  // Button toggling
  //

  private buttonsDisable() {
    for (const id in this.buttons) { // tslint:disable-line:forin
      this.buttons[id].setActive(false);
    }
  }

  private buttonsEnable() {
    for (const id in this.buttons) { // tslint:disable-line:forin
      this.buttons[id].setActive(true);
    }
  }

  //
  // Attribute getters
  //

  private getName(index: number): string {
    const type = this.getType(index);
    return CharacterNames[type];
  }

  private getHealth(index: number): number {
    const divisor: number = Math.pow(1000, index) * 100;
    return Math.floor(this.attributes / divisor) % 10;
  }

  private getPower(index: number): number {
    const divisor: number = Math.pow(1000, index) * 10;
    return Math.floor(this.attributes / divisor) % 10;
  }

  private getType(index: number): number {
    const divisor: number = Math.pow(1000, index);
    return Math.floor(this.attributes / divisor) % 10;
  }

  //
  // Level conclusions
  //

  private levelLose() {
    this.log("Everything goes dark...");
    new Tween(this)
      .to({ alpha: 0 }, 1000)
      .easing(Easing.Quadratic.In)
      .onComplete(() => {
        this.visible = false;
        this.alpha = 1;
        this.levelExit(false);
      })
      .start();
  }

  private levelWin() {
    this.log("Exiting level triumphantly...");
    new Tween(this)
      .to({ alpha: 0 }, 1000)
      .easing(Easing.Quadratic.In)
      .onComplete(() => {
        this.visible = false;
        this.alpha = 1;

        let levelsBeat = Number(window.localStorage.getItem("levelsBeat"));
        if (isNaN(levelsBeat)) { levelsBeat = 0; }

        let firstTime: boolean = false;
        if (this.level > levelsBeat) {
          firstTime = true;
          window.localStorage.setItem("levelsBeat", `${this.level}`);
        }
        this.levelExit(true, firstTime);
      })
      .start();
  }

  //
  // Logging
  //

  private log(message: string) {
    this.logs.push(message);
    if (this.logs.length > 4) {
      this.logs.shift();
    }
    this.updateStatusText();
  }

  //
  // HUD callbacks
  //

  private goSouth() {
    this.buttonsDisable();
    this.turnAttemptMove(0, 1);
  }

  private goWest() {
    this.buttonsDisable();
    this.turnAttemptMove(-1, 0);
  }

  private goEast() {
    this.buttonsDisable();
    this.turnAttemptMove(1, 0);
  }

  private goNorth() {
    this.buttonsDisable();
    this.turnAttemptMove(0, -1);
  }

  private goWait() {
    this.buttonsDisable();
    this.turnWait();
  }

  //
  // Map data & resource placement
  //

  private putAttributes(value: number) {
    this.attributes = value;
    this.updateStatusText();
  }

  private putCharacters(entityPositions: PIXI.Point[]) {
    // Clear old entities
    for (const entity of this.characters) {
      entity.destroy();
    }

    this.characters = [];
    for (let i = 0; i < entityPositions.length; i++) {
      const name: string = this.getName(i).toLowerCase();
      const entity: Entity = new Entity(name, entityPositions[i]);
      this.mapView.addChild(entity);
      this.characters.push(entity);
    }
  }

  private putExit(exit: PIXI.Point) {
    this.exit.setTile(exit);
  }

  private putItems(items: LEVEL.Item[]) {
    // Clear old items
    for (const entity of this.items) {
      entity.destroy();
    }

    for (const item of items) {
      const entity = new Entity(item.name, item.position);
      entity.name = item.name;
      this.mapView.addChild(entity);
      this.items.push(entity);
    }
  }

  private putTerrain(tiles: string[][]) {
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        this.tiles[y][x].texture = IMAGE.get(tiles[y][x]);
        this.walls[y][x] = tiles[y][x] === "wall";
      }
    }
  }

  //
  // Turn taking logic
  //
  private turnAttack(target: number, deltaX: number, deltaY: number) {
    this.log(`${this.getName(this.turn)} hits ${this.getName(target)} for ${this.getPower(this.turn)} damage`);

    // Attacker bounce back animation
    const cooldownX: number = this.characters[this.turn].position.x;
    const cooldownY: number = this.characters[this.turn].position.y;
    const cooldownTween: Tween = new Tween(this.characters[this.turn].position)
      .to({ x: cooldownX, y: cooldownY }, 200)
      .easing(Easing.Quadratic.Out)
      .onComplete(() => this.turnEnd());

    // Defender red blink
    const blinkTween: Tween = new Tween(0)
      .to(1, 200)
      .onUpdate((v: number) => {
        this.characters[target].tint = colorToTint(255, 255 * v, 255 * v);
      });

    // Defender bounce back
    const recoverX: number = this.characters[target].position.x;
    const recoverY: number = this.characters[target].position.y;
    const shovedX: number = recoverX + 8 * deltaX;
    const shovedY: number = recoverY + 8 * deltaY;
    const recoverTween: Tween = new Tween(this.characters[target].position)
      .to({ x: recoverX, y: recoverY }, 200)
      .easing(Easing.Quadratic.InOut);

    // Attacker lunge animation
    const attackX: number = this.characters[this.turn].position.x + (16 * deltaX);
    const attackY: number = this.characters[this.turn].position.y + (16 * deltaY);
    const attackTween: Tween = new Tween(this.characters[this.turn].position)
      .to({ x: attackX, y: attackY }, 200)
      .easing(Easing.Quadratic.In)
      .onComplete(() => {
        // Damage the target
        this.updateHealth(target, -this.getPower(this.turn));
        blinkTween.start();
        this.characters[target].position.set(shovedX, shovedY);

        if (this.getHealth(target) > 0) {
           // bounce back if the character is alive
           recoverTween.start();
        }

        cooldownTween.start();
      });

    attackTween.start();
  }

  private turnAttemptMove(deltaX: number, deltaY: number) {
    const targetX = this.characters[this.turn].tile.x + deltaX;
    const targetY = this.characters[this.turn].tile.y + deltaY;
    if ( (targetX < 0 || targetX > 8)
      || (targetY < 0 || targetY > 8)
      || (this.walls[targetY][targetX])
    ) {
      this.log(`${this.getName(this.turn)} hits a wall`);
      return this.turnWait();
    }

    // moveAttack
    for (let i = 0; i < this.characters.length; i++) {
      if ( (this.characters[i].tile.x === targetX)
        && (this.characters[i].tile.y === targetY)
        && (this.getHealth(i) > 0) // let's not collide with dead characters
      ) {
        return this.turnAttack(i, deltaX, deltaY);
      }
    }

    // move
    this.characters[this.turn].tile.set(targetX, targetY);

    // Test for item usage
    for (const item of this.items) {
      if (!item.visible) { continue; }
      if (item.tile.x !== targetX || item.tile.y !== targetY) { continue; }

      if (item.name === "restore") {
        this.log(`${this.getName(this.turn)} recovers 1 health`);
        this.updateHealth(this.turn, 1);
      }
      if (item.name === "acid") {
        this.log(`${this.getName(this.turn)} loses 1 health`);
        this.updateHealth(this.turn, -1);
      }
      if (item.name === "powerup") {
        this.log(`${this.getName(this.turn)} gains 1 power`);
        this.updatePower(this.turn, 1);
      }
      if (item.name === "weaken") {
        this.log(`${this.getName(this.turn)} loses 1 power`);
        this.updatePower(this.turn, -1);
      }
      if (item.name === "megacure") {
        this.log(`${this.getName(this.turn)} gains 5 health`);
        this.updateHealth(this.turn, 5);
      }
      if (item.name === "cursedglyph") {
        this.log(`${this.getName(this.turn)} loses 5 health`);
        this.updateHealth(this.turn, -5);
      }

      // Fade item
      new Tween(item)
        .to({ alpha: 0}, 200)
        .onComplete(() => item.visible = false)
        .start();
    }

    // Animate movement
    new Tween(this.characters[this.turn].position)
      .to({ x: targetX * 32, y: targetY * 32}, 300)
      .easing(Easing.Quadratic.Out)
      .onComplete(() => this.turnEnd())
      .start();
  }

  private turnEnd() {
    // Check exit
    if ( (this.turn === 0)
      && (this.characters[0].tile.x === this.exit.tile.x)
      && (this.characters[0].tile.y === this.exit.tile.y)
    ) {
      return this.levelWin();
    }

    this.turn = (this.turn + 1) % 5;

    // Skip dead characters
    if (this.getHealth(this.turn) === 0) {
      // Special case: player dead -> game over
      if (this.turn === 0) {
        this.levelLose();
        return;
      }
      this.turnEnd();
      return;
    }

    if (this.turn === 0) {
      // Player move - re-enable the buttons
      this.buttonsEnable();
    } else {
      // Enemy move - advanced AI decides
      this.turnEnemy();
    }
  }

  private turnEnemy() {
    // Warning: complex AI ahead - do not steal
    const enemyX: number = this.characters[this.turn].tile.x;
    const enemyY: number = this.characters[this.turn].tile.y;
    const playerX: number = this.characters[0].tile.x;
    const playerY: number = this.characters[0].tile.y;

    if (enemyX > playerX && this.walls[enemyY][enemyX - 1] === false) {
      this.turnAttemptMove(-1, 0);
    } else if (enemyX < playerX && this.walls[enemyY][enemyX + 1] === false) {
      this.turnAttemptMove(1, 0);
    } else if (enemyY > playerY) {
      this.turnAttemptMove(0, -1);
    } else if (enemyY < playerY) {
      this.turnAttemptMove(0, 1);
    } else {
      this.turnWait();
    }
  }

  private turnWait() {
    this.characters[this.turn].scale.set(1.2, 1.2);
    new Tween(this.characters[this.turn].scale)
      .to({ x: 1, y: 1}, 300)
      .easing(Easing.Quadratic.Out)
      .onComplete(() => this.turnEnd())
      .start();
  }

  //
  // Attribute & status bar updating
  //

  private updatedAttributes() {
    for (let i = 0; i < this.characters.length; i++) {
      // Update character appearances if some STRANGE and UNEXPLAINABLE bug has
      // changed the character :3
      const name = this.getName(i).toLowerCase();
      if (name !== "") {
        this.characters[i].texture = IMAGE.get(name);
      }

      // Fade away dead characters
      if (this.getHealth(i) === 0 && this.characters[i].visible) {
        new Tween(this.characters[i])
          .to({ alpha: 0 }, 200)
          .onComplete(() => this.characters[i].visible = false)
          .start();
      }

      // If a living character was accidentally made invisible, make it visible
      // god these bugs are so strange :P :P :P
      if (this.getHealth(i) > 0) {
        this.characters[i].visible = true;
      }
    }

    this.updateStatusText();
  }

  private updateHealth(index: number, delta: number) {
    const oldHealth = this.getHealth(index);
    let newHealth = oldHealth + delta;

    // Let's ensure we don't get negative health values here, we want to protect
    // our code against bugs :^)
    if (newHealth < 0) { newHealth = 0; }

    const healthFactor = Math.pow(1000, index) * 100;

    // We subtract the old health from the collective attributes number
    this.attributes -= oldHealth * healthFactor;
    // We then add the new health to the collective attributes number
    this.attributes += newHealth * healthFactor;

    this.updatedAttributes();
  }

  private updatePower(index: number, delta: number) {
    const oldPower = this.getHealth(index);
    let newPower = oldPower + delta;

    // Let's ensure we don't get negative power values here, we want to protect
    // our code against bugs :o)
    if (newPower < 0) { newPower = 0; }

    const powerFactor = Math.pow(1000, index) * 10;

    // We subtract the old power from the collective attributes number
    this.attributes -= oldPower * powerFactor;
    // We then add the new power to the collective attributes number
    this.attributes += newPower * powerFactor;

    this.updatedAttributes();
  }

  private updateStatusText() {
    this.statusBarText.text = `
TYPE POWER HEALTH CHARACTER
___________________________

  ${this.getType(0)}     ${this.getPower(0)}     ${this.getHealth(0)}   ${this.getName(0)}
  ${this.getType(1)}     ${this.getPower(1)}     ${this.getHealth(1)}   ${this.getName(1)}
  ${this.getType(2)}     ${this.getPower(2)}     ${this.getHealth(2)}   ${this.getName(2)}
  ${this.getType(3)}     ${this.getPower(3)}     ${this.getHealth(3)}   ${this.getName(3)}
  ${this.getType(4)}     ${this.getPower(4)}     ${this.getHealth(4)}   ${this.getName(4)}
___________________________

Latest messages:

${this.logs.join("\n")}
`;
  }
  // updateStatusText()
}
