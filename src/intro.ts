// External Libraries
import * as PIXI from "pixi.js";
import { Easing, Tween } from "tween.js";

import * as IMAGE from "./image";

export class Intro extends PIXI.Container {
  private caveBright: PIXI.Sprite;
  private caveDark: PIXI.Sprite;
  private knightBright: PIXI.Sprite;
  private knightDark: PIXI.Sprite;

  private onEnd: () => void;

  constructor(onEnd: () => void) {
    super();

    this.onEnd = onEnd;

    // load images
    this.caveDark = new PIXI.Sprite(IMAGE.get("intro_cave_dark"));
    this.caveBright = new PIXI.Sprite(IMAGE.get("intro_cave_bright"));
    this.knightDark = new PIXI.Sprite(IMAGE.get("intro_knight_dark"));
    this.knightBright = new PIXI.Sprite(IMAGE.get("intro_knight_bright"));

    // Add children
    this.addChild(this.caveDark);
    this.addChild(this.caveBright);
    this.addChild(this.knightDark);
    this.addChild(this.knightBright);

    // Fix anchors
    this.caveDark.anchor.set(0.5, 0.5);
    this.caveBright.anchor.set(0.5, 0.5);
    this.knightDark.anchor.set(0.5, 0.5);
    this.knightBright.anchor.set(0.5, 0.5);

    // Hide everything
    this.caveDark.alpha = 0;
    this.caveBright.alpha = 0;
    this.knightDark.alpha = 0;
    this.knightBright.alpha = 0;

    this.interactive = true;
    this.on("pointerdown", () => this.finish());
  }

  public play() {
    console.log("intro start");
    this.alpha = 1;
    setTimeout(() => this.fadeInCave(), 1000);
  }

  private fadeInCave() {
     new Tween(this.caveDark)
      .to({ alpha: 1 }, 500)
      .easing(Easing.Quadratic.InOut)
      .onComplete(() => this.showCave())
      .start();
  }

  private showCave() {
    setTimeout(() => this.flashCave(true), 1500);
  }

  private flashCave(repeat: boolean) {
    this.caveBright.alpha = 1;
    new Tween(this.caveBright)
      .to({ alpha: 0 }, 500)
      .easing(Easing.Quadratic.Out)
      .onComplete(() => {
        if (repeat) {
          setTimeout(() => this.flashCave(false), 1000);
        } else {
          setTimeout(() => this.fadeOutCave(), 500);
        }})
      .start();
  }

  private fadeOutCave() {
    new Tween(this.caveDark)
      .to({ alpha: 0 }, 500)
      .easing(Easing.Quadratic.InOut)
      .onComplete(() => this.fadeInKnight())
      .start();
  }

  private fadeInKnight() {
    new Tween(this.knightDark)
      .to({ alpha: 1 }, 500)
      .easing(Easing.Quadratic.InOut)
      .onComplete(() => this.showKnight())
      .start();
  }

  private showKnight() {
    setTimeout(() => this.flashKnight(true), 1000);
  }

  private flashKnight(repeat: boolean) {
    this.knightBright.alpha = 1;
    new Tween(this.knightBright)
      .to({ alpha: 0 }, 500)
      .easing(Easing.Quadratic.Out)
      .onComplete(() => {
        if (repeat) {
          setTimeout(() => this.flashKnight(false), 1500);
        } else {
          setTimeout(() => this.fadeOutKnight(), 500);
        }})
      .start();
  }

  private fadeOutKnight() {
    new Tween(this.knightDark)
      .to({ alpha: 0 }, 500)
      .easing(Easing.Quadratic.InOut)
      .onComplete(() => this.finish())
      .start();
  }

  private finish() {
    if (this.visible === false) {
      return;
    }
    this.visible = false;
    this.onEnd();
  }
}
