// External Libraries
import * as PIXI from "pixi.js";

// Game libraries
import { ButtonTextStyle } from "./textStyle";

export type ButtonCallback = () => void;

const ColorActiveFrame: number         = 0xffffff;
const ColorActiveFrameBorder: number   = 0xaaaaaa;
const ColorPressedFrame: number        = 0x000000;
const ColorPressedFrameBorder: number  = 0x444444;
const ColorInactiveFrame: number       = 0x777777;
const ColorInactiveFrameBorder: number = 0x666666;

export class Button extends PIXI.Container {
  // PIXI resources:
  private text: PIXI.Text;
  private backgroundFrames: { [state: string]: PIXI.Graphics } = {};

  private active: boolean = true;
  private state: string;
  private callback: ButtonCallback;

  private bText: string;
  private bWidth: number;
  private bHeight: number;

  constructor(callback: ButtonCallback, width: number, height: number, text: string) {
    super();

    // Grab button
    this.bWidth = width;
    this.bHeight = height;
    this.bText = text;

    // Set default state
    this.state = "active";

    // Handle mouse events
    this.interactive = true;
    this.on("pointerdown", this.press);
    this.on("pointerup", this.release);
    this.on("pointerout", this.release);
    this.callback = callback;

    this.makeFrames();

    this.text = new PIXI.Text(this.bText, ButtonTextStyle);
    this.text.position.set(0, 0);
    this.text.anchor.set(0.5, 0.5);
    this.addChild(this.text);
    this.text.position.set(0.5 * this.bWidth, 0.5 * this.bHeight);
  }

  public resize(width: number, height: number) {
    this.bWidth = width;
    this.bHeight = height;

    // Let's ensure the text stays on top
    this.removeChild(this.text);
    this.destroyFrames();
    this.makeFrames();
    this.addChild(this.text);
    this.text.position.set(0.5 * this.bWidth, 0.5 * this.bHeight);
  }

  public setActive(active: boolean) {
    this.active = active;
    if (this.state === "active" && !active) {
      this.setState("inactive");
    } else if (this.state === "inactive" && active) {
      this.setState("active");
    }
  }

  private destroyFrames() {
    this.removeChild(this.backgroundFrames.active);
    this.backgroundFrames.active.destroy();

    this.removeChild(this.backgroundFrames.inactive);
    this.backgroundFrames.inactive.destroy();

    this.removeChild(this.backgroundFrames.pressed);
    this.backgroundFrames.pressed.destroy();
  }

  private makeFrames() {
    // Build new frames
    this.backgroundFrames = {
      active:   this.makeFrame(ColorActiveFrame, ColorActiveFrameBorder),
      inactive: this.makeFrame(ColorInactiveFrame, ColorInactiveFrameBorder),
      pressed:  this.makeFrame(ColorPressedFrame, ColorPressedFrameBorder),
    };

    // Adjust resource anchors
    // this.backgroundFrames.active.pivot.set(0.5 * this.bWidth, 0.5 * this.bHeight);
    // this.backgroundFrames.inactive.pivot.set(0.5 * this.bWidth, 0.5 * this.bHeight);
    // this.backgroundFrames.pressed.pivot.set(0.5 * this.bWidth, 0.5 * this.bHeight);

    // Set default frame visibility
    this.backgroundFrames.active.visible   = this.state === "active";
    this.backgroundFrames.inactive.visible = this.state === "inactive";
    this.backgroundFrames.pressed.visible  = this.state === "pressed";

    // Add to hierarchy
    this.addChild(this.backgroundFrames.active);
    this.addChild(this.backgroundFrames.inactive);
    this.addChild(this.backgroundFrames.pressed);
  }

  private makeFrame(backgroundColor: number, borderColor: number): PIXI.Graphics {
    const frame = new PIXI.Graphics();

    // Draw rectangle
    frame.lineStyle(2, borderColor, 1);
    frame.beginFill(backgroundColor);
    frame.drawRect(0, 0, this.bWidth, this.bHeight);
    // console.log("drawRect(0, 0,", this.bWidth, ",", this.bHeight, ")");
    frame.endFill();

    return frame;
  }

  private press() {
    if (this.state !== "active") { return; }
    this.callback();
    this.setState("pressed");
  }

  private release() {
    if (this.state !== "pressed") { return; }
    if (this.active) {
      this.setState("active");
    } else {
      this.setState("inactive");
    }
  }

  private setState(state: string) {
    if (this.state === state) { return; }
    // Hide old frame
    this.backgroundFrames[this.state].visible = false;

    // Set new state graphics
    this.state = state;
    this.backgroundFrames[this.state].visible = true;
  }
}
