// External Libraries
import * as PIXI from "pixi.js";
// import * as TWEEN from "tween.js"

// Game libraries
import * as IMAGE from "./image";

export class Entity extends PIXI.Sprite {
  public tile: PIXI.Point;

  constructor(name: string, tile: PIXI.Point) {
    super(IMAGE.get(name));
    this.tile = tile;
    this.position.set(tile.x * 32, tile.y * 32);
  }

  public destroy() {
    if (this.parent) {
      this.parent.removeChild(this);
    }
    // does not calling super.destroy() cause the sprite to linger in memory?
    // is it even a noticeable memory leak size?
    // somehow the following call is causing errors about textures being null,
    // which wouldn't be too surprising considering all the silly things I'm
    // doing in this code.
    // super.destroy();
  }

  public setTile(tile: PIXI.Point) {
    this.tile.set(tile.x, tile.y);
    this.position.set(tile.x * 32, tile.y * 32);
  }
}
