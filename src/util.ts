export function colorToTint(r: number, g: number, b: number): number {
  const rComponent = Math.floor(r) * 256 * 256;
  const gComponent = Math.floor(g) * 256;
  const bComponent = Math.floor(b);
  return rComponent + gComponent + bComponent;
}
