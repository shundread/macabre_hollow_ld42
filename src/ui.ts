// External Libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Game Libraries
import { Game } from "./game";
import { Intro } from "./intro";
import { lastLevelIndex } from "./level";
import { MainMenu } from "./mainMenu";
import { TransitionTextStyle } from "./textStyle";

let renderer: PIXI.SystemRenderer;
let stage: PIXI.Container;

let game: Game;
let intro: Intro;
let mainMenu: MainMenu;
let transitionText: PIXI.Text;

export function initialize() {
  renderer = PIXI.autoDetectRenderer(
    document.body.clientWidth,
    document.body.clientHeight,
    { roundPixels: true });

  // Directly from PIXI tutorial https://github.com/kittykatattack/learningPixi
  renderer.view.style.position = "absolute";
  renderer.view.style.display = "block";
  document.body.appendChild(renderer.view);

  // Instance main menu & game
  game = new Game(
    document.body.clientWidth,
    document.body.clientHeight,
    levelExit);
  mainMenu = new MainMenu(
    document.body.clientWidth,
    document.body.clientHeight,
    levelEnter);
  intro = new Intro(introOver);
  intro.position.set(
    0.5 * document.body.clientWidth,
    0.5 * document.body.clientHeight);
  transitionText = new PIXI.Text("", TransitionTextStyle);
  transitionText.anchor.set(0.5, 0.5);
  transitionText.position.set(
    0.5 * document.body.clientWidth,
    0.5 * document.body.clientHeight);
  transitionText.visible = false;

  // Top-level layout hierarchy
  stage = new PIXI.Container();
  stage.addChild(intro);
  stage.addChild(mainMenu);
  stage.addChild(game);
  stage.addChild(transitionText);
  intro.play();

  // Add resize handlers for phone users :)
  // also, I sunk too many hours in putting placeholder code for responsiveness
  // not to add this feature not that it's nearly free at this point
  window.addEventListener("resize", handleResize);

  // Initialize render loop
  PIXI.ticker.shared.add(tick);
}

function handleResize() {
  renderer.resize(document.body.clientWidth, document.body.clientHeight);
  intro.position.set(
    0.5 * document.body.clientWidth,
    0.5 * document.body.clientHeight);
  transitionText.position.set(
    0.5 * document.body.clientWidth,
    0.5 * document.body.clientHeight);
  mainMenu.resize(document.body.clientWidth, document.body.clientHeight);
  game.resize(document.body.clientWidth, document.body.clientHeight);
}

function introOver() {
  mainMenu.show();
}

function levelEnter() {
  // Transition into the level
  transition(
    "You delve into the darkness...",
    () => game.enterLevel(mainMenu.levelSelected));

}

function levelExit(success: boolean, firstTime: boolean = false) {
  let message: string;
  if (success) {
    if (firstTime) {
      if (mainMenu.levelSelected === lastLevelIndex()) {
        message = "You conquer the depths\nof Macabre Hollow";
      } else {
        message = "The next level opens up...";
      }
    } else {
      message = "You return triumphant...";
    }
  } else {
    message = "The depths swallow you...";
  }

  // Bump up current level if we woon and if there are any new levels left
  if (success && mainMenu.levelSelected < lastLevelIndex()) {
    mainMenu.levelSelected += 1;
  }

  // Transition to main menu
  transition(message, () => mainMenu.show());
}

function tick() {
  renderer.render(stage);
  TWEEN.update();
}

function transition(message: string, followUp: () => void) {
  transitionText.text = message;
  transitionText.alpha = 0;
  transitionText.visible = true;

  const fadeIn: TWEEN.Tween = new TWEEN.Tween(transitionText)
    .to({ alpha: 1 }, 250);
  const linger: TWEEN.Tween = new TWEEN.Tween(0)
    .to(1, 1500);
  const fadeOut: TWEEN.Tween = new TWEEN.Tween(transitionText)
    .to({ alpha: 0 }, 250)
    .onComplete(() => {
      transitionText.visible = false;
      followUp();
    });
  fadeIn.chain(linger);
  linger.chain(fadeOut);
  fadeIn.start();
}
