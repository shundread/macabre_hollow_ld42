// External libraries
import * as PIXI from "pixi.js";

const item: { [name: string]: PIXI.Texture } = {};

export function parseImages(): void {
  console.log("Loading images");
  console.log(PIXI.loader.resources);

  const spritesResource = PIXI.loader.resources["dist/sprites.json"];
  if (!spritesResource.textures) {
    throw Error("Unable to grab sprites resource");
  }
  for (const id in spritesResource.textures) { // tslint:disable-line:forin
    item[trimmedId(id)] = spritesResource.textures[id];
    console.log(`${trimmedId(id)} is`, item[trimmedId(id)]);
  }
}

export function get(name: string): PIXI.Texture {
  return item[name];
}

function trimmedId(id: string): string {
  return id.split(".png")[0];
}
