// External Libraries
import * as UI from "./ui";

// Game libraries
import * as IMAGE from "./image";

function boot() {
  document.removeEventListener("DOMContentLoaded", boot);
  PIXI.ticker.shared.stop();
  PIXI.loader
    .add("dist/sprites.json")
    .load(onDataLoaded);
}

function onDataLoaded() {
  IMAGE.parseImages();
  initialize();
}

function initialize() {
  UI.initialize();
}

document.addEventListener("DOMContentLoaded", boot);
