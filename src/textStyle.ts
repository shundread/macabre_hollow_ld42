import * as PIXI from "pixi.js";

export const ButtonTextStyle = new PIXI.TextStyle({
  align:           "center",
  fill:            "#dddddd",
  fontFamily:      "monospace",
  fontSize:        "13px",
  fontWeight:      "bold",
  stroke:          "#000000",
  strokeThickness: 2,
});

export const InstructionsTextStyle = new PIXI.TextStyle({
  align:           "left",
  fill:            "#000000",
  fontFamily:      "monospace",
  fontSize:        "10px",
});

export const MenuTextStyle = new PIXI.TextStyle({
  align:           "center",
  fill:            "#000000",
  fontFamily:      "monospace",
  fontSize:        "16px",
  fontWeight:      "bold",
  stroke:          "#ffffff",
  strokeThickness: 3,
});

export const StatusBarTextStyle = new PIXI.TextStyle({
  align:           "left",
  fill:            "#dddddd",
  fontFamily:      "monospace",
  fontSize:        "8px",
});

export const TransitionTextStyle = new PIXI.TextStyle({
  align:           "center",
  fill:            "#ff938c",
  fontFamily:      "monospace",
  fontSize:        "18px",
  stroke:          "#4b0600",
  strokeThickness: 3,
});
